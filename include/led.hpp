
constexpr unsigned long blinking_interval = 250;

struct LEDState {
  int r, g, b;
  LEDState() : r(0), g(0), b(0) {}
  LEDState(byte r, byte g, byte b) : r(r), g(g), b(b) {}
  static void write(byte r, byte g, byte b) {
    LEDState(r, g, b).commit();
  }
  enum pins {
    red = 9,
    green = 6,
    blue = 5,
    base = 7
  };
  void _write() {
    constexpr int M = (1<<8)-1;
    analogWrite(pins::red, M^r);
    analogWrite(pins::green, M^g);
    analogWrite(pins::blue, M^b);
  }
  void commit() {
    turnon();
    _write();
  }
  static void turnoff() {
    digitalWrite(pins::base, LOW);
  }
  static void turnon() {
    digitalWrite(pins::base, HIGH);
  }
  static void init() {
    pinMode(pins::red, OUTPUT);
    pinMode(pins::green, OUTPUT);
    pinMode(pins::blue, OUTPUT);
    pinMode(pins::base, OUTPUT);
    write(0, 0, 0);
    turnon();
  }
  void flash() {
    /*
     * Blocking flash to indicate accepted command
     */
    _write();
    turnon();
    delay(50);
    turnoff();
  }
};

namespace led_states {
  const LEDState
    off     {0  , 0  , 0  },
    warning {128, 128, 0  },
    okay    {0  , 128, 0  },
    white   {128, 128, 128},
    red     {128, 0  , 0  },
    purple  {128, 0  , 128},
    blue    {0  , 0  , 128},
    diving  {0  , 8  , 32 },
    floating{20 , 30 , 10 },
    commwait{40 , 0  , 128};
};
