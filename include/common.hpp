
enum class ServoDirection {
  blow = 180,
  suck = 0
};

enum Pins : int {
  servo_pin = 10,
  end_trigger_pin = 11,
  ir_recv_pin = 2,
  ir_gnd_pin = 3,
  ir_pwr_pin = 4,
};

enum AnalogPins : int {
  pot_pin = A3
};

enum class PistonPosition {
  unknown,
  blown,
  sucked
} piston_position = PistonPosition::unknown;

unsigned long dive_float_time = 10000; // ms
