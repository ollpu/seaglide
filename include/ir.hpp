
// Ir commands should be non-blockingly checked for
// ~2 seconds after every rise
// The led could indicate that the glider is able to
// receive commands

IRrecv irrecv(ir_recv_pin);
decode_results results;
unsigned long last_cmd = 0;

enum IRCommands : unsigned long {
  VLOLDOWN = 0xFD00FF, // intentional meme
  PAUSE    = 0xFD807F,
  VOLUP    = 0xFD40BF,
  SETUP    = 0xFD20DF,
  UP       = 0xFDA05F,
  STOP     = 0xFD609F,
  LEFT     = 0xFD10EF,
  ENTER    = 0xFD906F,
  RIGHT    = 0xFD50AF,
  ZERO     = 0xFD30CF,
  DOWN     = 0xFDB04F,
  RETURN   = 0xFD708F
};

void init_communication() {
  irrecv.enableIRIn();
  pinMode(ir_gnd_pin, OUTPUT);
  pinMode(ir_pwr_pin, OUTPUT);
  digitalWrite(ir_gnd_pin, LOW);
  digitalWrite(ir_pwr_pin, HIGH);
}


void communication();

void start_communication(unsigned long wait_time) {
  unsigned long start_time = millis();
  //unsigned long wait_time = 5000 + dive_float_time;
  bool do_start = false;
  led_states::commwait.commit();
  irrecv.resume();
  while (true) {
    if (millis()-start_time > wait_time) break;
    if (irrecv.decode(&results)) {
      if (results.value == PAUSE) {
        led_states::blue.flash();
        do_start = true;
        break;
      }
      irrecv.resume();
    }
  }
  irrecv.resume();
  if (do_start) {
    // Enter communication state
    Serial.println("Started communication");
    communication();
    Serial.println("Ended communication");
  }
}

void communication() {
  while (true) {
    if (irrecv.decode(&results)) {
      Serial.println("Remote cmd");
      led_states::blue.flash();
      // When pause (play) is pressed, return to normal operation
      if (results.value == PAUSE) {
        led_states::red.flash();
        Serial.println("Exit");
        return;
      }
      // Increase dive time
      if (results.value == RIGHT ||
         (last_cmd == RIGHT && results.value == REPEAT)) {
        led_states::red.flash();
        dive_float_time += 1000;
        last_cmd = RIGHT;
        Serial.println("Inc");
      }
      // Decrease dive time
      if (results.value == LEFT ||
         (last_cmd == LEFT && results.value == REPEAT)) {
        if (dive_float_time >= 2000) {
          led_states::red.flash();
          dive_float_time -= 1000;
          Serial.println("dec");
        } else {
          led_states::purple.flash();
          delay(100);
          led_states::purple.flash();
        }
        last_cmd = LEFT;
      }
      // Blinks white as many times as the number of tens of seconds
      // then blinks green as many times as the number of remaining seconds
      if (results.value == ENTER) {
        delay(400);
        for (int i = 0; i < dive_float_time / 10000; i++) {
          led_states::white.flash();
          delay(150);
        }
        delay(400);
        for (int i = 0; i < (dive_float_time % 10000) / 1000; i++) {
          led_states::okay.flash();
          delay(150);
        }
      }
      irrecv.resume();
    }
  }
}
