
#include <IRremote.h>
#include <Servo.h>

#include "include/common.hpp"
#include "include/led.hpp"
#include "include/ir.hpp"

Servo servo;

long last_float_start = 0;

void setup() {
  Serial.begin(9600);
  pinMode(pot_pin, INPUT);
  pinMode(servo_pin, OUTPUT);
  pinMode(end_trigger_pin, INPUT_PULLUP);

  LEDState::init();
  init_communication();
  home_motor();
  start_communication(5000);
}

void loop(){
  start_float();
  led_states::floating.commit();
  delay(dive_float_time);
  
  start_communication(5000 + dive_float_time);

  start_dive();
  led_states::diving.commit();
  delay(dive_float_time);
}

void start_motor(ServoDirection dir) {
  /*
   * Starts spinning the motor continuously in the given
   * direction
   */
  if (!servo.attached()) servo.attach(servo_pin);
  int val = static_cast<int>(dir);
  if (servo.read() != val) servo.write(val);
}

void stop_motor() {
  /*
   * Detaches the motor's pwm signal
   */
  if (servo.attached()) servo.detach();
}

void home_motor() {
 /*
  * Tries to dive until the piston reaches the endstop
  * Blinks while homing
  * NOTE: This should be done before placing the glider in water
  */
  Serial.println("Homing!");
  if (digitalRead(end_trigger_pin)) {
    start_motor(ServoDirection::suck);
    led_states::red.commit();
    unsigned long last_time = millis(), time;
    bool led_toggle = false;
    while (digitalRead(end_trigger_pin)) {
      time = millis();

      if (time - last_time > blinking_interval) {
        if (led_toggle) {
          led_states::red.commit();
        } else {
          led_states::off.commit();
        }
        led_toggle ^= true;
        last_time = time;
      }
    }
    stop_motor();
  }
  piston_position = PistonPosition::sucked;
  led_states::okay.commit();
}

void start_dive() {
 /*
  * Starts the dive if the seaglide is floating
  * Blinks while the piston is moving
  */
  if (piston_position == PistonPosition::blown)
  {
    Serial.println("Diving!");

    start_motor(ServoDirection::suck);
    unsigned long last_time = millis(), time;
    bool led_is_on = false;
    while (digitalRead(end_trigger_pin)) {
      time = millis();

     // if (time - last_time > 2*blinking_interval) {
     //   led_states::off.commit();
     //   last_time = time;
     //   led_is_on = false;
     // } else if (time - last_time > blinking_interval && !led_is_on) {
     //   led_states::warning.commit();
     //   led_is_on = true;
     // }
    }
    stop_motor();
    piston_position = PistonPosition::sucked;
    led_states::okay.commit();
  } else {
    Serial.println("Float first!");
    led_states::red.flash();
    delay(50);
    led_states::red.flash();
    delay(50);
    led_states::red.flash();
  }
}

void start_float() {
 /*
  * Starts the floating procedure if
  * the seaglide is diving
  * Blinks while the motor is moving
  */
  if (piston_position == PistonPosition::sucked) {
    Serial.println("Floating!");

    start_motor(ServoDirection::blow);
    unsigned long last_time = millis(), time, start_time = last_time;
    bool led_is_on = false;
    while (millis() - start_time < 14000) {
      time = millis();

     //  if (time - last_time > 2*blinking_interval) {
     //    led_states::off.commit();
     //    last_time = time;
     //    led_is_on = false;
     //  } else if (time - last_time > blinking_interval && !led_is_on) {
     //    led_states::warning.commit();
     //    led_is_on = true;
     //  }
    }
    led_states::off.commit();
    stop_motor();
    piston_position = PistonPosition::blown;
    led_states::okay.commit();
  } else {
    Serial.println("Home or dive first!");
    led_states::red.flash();
    delay(50);
    led_states::red.flash();
    delay(50);
    led_states::red.flash();
  }
}                                                                               // EOM (end of method)
